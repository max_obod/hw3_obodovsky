# Сформировать строку, в которой содержится информация об определенном слове в строке.
# Например "The [номер] symbol in [тут слово] is [значение символа по номеру в слове]".
# Слово и номер получите с помощью input() или воспользуйтесь константой.
# Например (если слово - "Python" а символ 3) - "The 3 symbol in "Python" is t".

while True:
    word = input("Enter the word: ")

    while True:
        try:
            number = int(input("Enter the number: "))
            break
        except ValueError:
            print("NUMBER!")

    try:
        answer = word[number -1]
        break
    except IndexError:
        print("Number < len(word)")


print(f"The {number} symbol in {word} is {answer.rstrip('-')}")


# Ввести из консоли строку. Определить количество слов в этой строке,
# которые заканчиваются на букву "o" (учтите, что буквы бывают заглавными).

user_input = input("Enter the Line: ")
user_input_split = user_input.split()
counter = 0
for wrd in user_input_split:
    if wrd.endswith("o") or wrd.endswith("O"):
        counter += 1
print(counter)



# Есть list с данными lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum'].
# Напишите механизм, который формирует новый list (например lst2), который содержит только переменные-строки,
# которые есть в lst1.

lst1 = ['1', '2', 3, True, 'False', 5, '6', 7, 8, 'Python', 9, 0, 'Lorem Ipsum']
lst2 = list()

for element in lst1:
    if type(element) == str:
        lst2.append(element)
print(lst2)






